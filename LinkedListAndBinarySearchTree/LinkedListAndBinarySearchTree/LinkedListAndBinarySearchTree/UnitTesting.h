// unit.hpp

#ifndef UINT_HPP_INCLUDED
#define UINT_HPP_INCLUDED

#include <iostream>
#include <string>

template <class T>
bool are_equal(T a, T b)
{
	return a == b;
}

template <class T>
bool are_equal(LinkedList<T>& a, LinkedList<T>& b)
{
	if (a.Size() != b.Size())
		return false;

	bool equal = true;

	for (int i = 0; i < a.Size(); i++)
	{
		int aValue = a.At(i);
		int bValue = b.At(i);

		if (aValue != bValue)
		{
			equal = false;
			break;
		}
	}

	return equal;
}

template <class T>
bool verify(T expected, T got, const std::string& message)
{
	if (are_equal(expected, got))
	{
		std::cout << "Passed: " << message << std::endl;
		return true;
	}
	std::cout << "Failed! Expected: " << expected << " Got: " << got <<
		" - " << message << std::endl << std::endl;
	return false;
}

template <class T>
bool verify(LinkedList<T>& expected, LinkedList<T>& got, const std::string& message)
{
	if (are_equal(expected, got))
	{
		std::cout << "Passed: " << message << std::endl;
		return true;
	}
	std::cout << std::endl << "Failed! - " << message << std::endl << "Expected: " << std::endl;
	expected.PrintList();
	std::cout << std::endl << "Got: " << std::endl;
	got.PrintList();
	std::cout << std::endl << std::endl;
	return false;
}

#endif // UINT_HPP_INCLUDED