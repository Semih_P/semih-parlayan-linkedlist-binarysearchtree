template <class TValue>
class Node
{
public:
	Node(TValue v, Node<TValue>* rightNode = nullptr, Node<TValue>* leftNode = nullptr)
	{
		value = v;
		this->rightNode = rightNode;
		this->leftNode = leftNode;
	}
	~Node()
	{

	}

	TValue value;
	Node<TValue>* leftNode;
	Node<TValue>* rightNode;
};