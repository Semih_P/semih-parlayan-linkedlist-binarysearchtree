// LinkedListAndBinarySearchTree.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "LinkedList.h"
#include "BinarySearchTree.h"
#include "UnitTesting.h"
#include <vld.h>

template<class T>
void UnitTestLinkedList(LinkedList<T>& list);
template<class T>
void UnitTestBinarySearchTree(BinarySearchTree<T>& tree);


int _tmain(int argc, _TCHAR* argv[])
{
	//LinkedList
	LinkedList<int> list;
	list.PushBack(1);
	list.PushBack(2);
	list.PushBack(3);
	list.PushFront(10);
	list.PushFront(11);
	list.PushFront(12);

	UnitTestLinkedList(list);

	//BinarySearchTree
	BinarySearchTree<int> tree;
	tree.Insert(6);
	tree.Insert(3);
	tree.Insert(4);
	tree.Insert(5);
	tree.Insert(1);
	tree.Insert(13);
	tree.Insert(9);
	tree.Insert(16);
	tree.Insert(15);
	tree.Insert(22);

	UnitTestBinarySearchTree(tree);

	system("pause");
	return 0;
}

template<class T>
void UnitTestLinkedList(LinkedList<T>& list)
{
	//Unit testing begins here
	cout << std::endl << "Unit testing for LinkedList Started" << std::endl;
	std::cout << "------------------------------------------------------" << std::endl;

	//Size()
	verify<int>(6, list.Size(), "Size()		| LinkedList");

	//PushFront()
	list.PushFront(20);
	verify<int>(20, list.First(), "PushFront()	| LinkedList");

	//PushBack()
	list.PushBack(90);
	verify<int>(90, list.Last(), "PushBack()	| LinkedList");

	//PopFront()
	list.PopFront();
	verify<int>(12, list.First(), "PopFront()	| LinkedList");

	//PopBack()
	list.PopBack();
	verify<int>(3, list.Last(), "PopBack()	| LinkedList");

	//Clear()
	list.Clear();
	verify<int>(0, list.Size(), "Clear()		| LinkedList");


	std::cout << "------------------------------------------------------" << std::endl;
}

template<class T>
void UnitTestBinarySearchTree(BinarySearchTree<T>& tree)
{
	//Unit testing begins here
	cout << std::endl << "Unit testing for BinarySearchTree Started" << std::endl;
	std::cout << "------------------------------------------------------" << std::endl;

	//Insert() & FindValue()
	verify<int>(true, tree.FindValue(9), "Insert() & FindValue()	| BinarySearchTree");

	//Size()
	verify<int>(10, tree.Size(), "Size()			| BinarySearchTree");

	////Traversal In Order
	LinkedList<T> t;
	t.PushBack(1); t.PushBack(3); t.PushBack(4); t.PushBack(5); t.PushBack(6);
	t.PushBack(9); t.PushBack(13); t.PushBack(15); t.PushBack(16); t.PushBack(22);

	LinkedList<T> result;
	tree.TraversalInOrder(result);
	verify<int>(t, result, "Traversal In Order()	| BinarySearchTree");

	//Traversal Pre order
	result.Clear();
	t.Clear();
	t.PushBack(6); t.PushBack(3); t.PushBack(1); t.PushBack(4); t.PushBack(5);
	t.PushBack(13); t.PushBack(9); t.PushBack(16); t.PushBack(15); t.PushBack(22);

	tree.TraversalPreOrder(result);
	verify<int>(t, result, "Traversal Pre Order()	| BinarySearchTree");

	//Traversal Post order
	result.Clear();
	t.Clear();
	t.PushBack(1); t.PushBack(5); t.PushBack(4); t.PushBack(3); t.PushBack(9); t.PushBack(15);
	t.PushBack(22); t.PushBack(16); t.PushBack(13); t.PushBack(6);

	tree.TraversalPostOrder(result);
	verify<int>(t, result, "Traversal Pre Order()	| BinarySearchTree");

	//Clear()
	tree.Clear();
	verify<int>(0, tree.Size(), "Clear()			| BinarySearchTree");

	std::cout << "------------------------------------------------------" << std::endl;
}

