template <class T>
class BinarySearchTree
{
public:
	BinarySearchTree()
	{
		m_size = 0;
		m_root = nullptr;
	}
	~BinarySearchTree()
	{
		Clear();
	}

	//Insert
	void Insert(T value)
	{
		m_size++;

		//Add as m_root if there was no m_root
		if (m_root == nullptr)
		{
			m_root = new Node<T>(value);
			return;
		}

		InsertLocal(m_root, value);
	}

	//Clear
	void Clear()
	{
		if (m_root == nullptr)
		{
			return;
		}

		RemoveLocal(m_root);
		m_root = nullptr;
		m_size = 0;
	}

	//FindValue
	bool FindValue(T value)
	{
		//Return if there is no m_root
		if (m_root == nullptr)
			return false;

		Node<T>* currentNode = m_root;

		while (true)
		{
			if (currentNode->value == value)
			{
				return true;
			}
			else
			{
				currentNode = (value < currentNode->value) ? currentNode->leftNode : currentNode->rightNode;

				if (currentNode == nullptr)
					return false;
			}
		}
	}

	//Size
	int Size()
	{
		return m_size;
	}

	//Traversal methods
	void TraversalPreOrder(LinkedList<T>& list)
	{
		if (m_root == nullptr)
			return;

		PreOrderLocal(list, m_root);
	}
	void TraversalInOrder(LinkedList<T>& list)
	{
		if (m_root == nullptr)
			return;

		InOrderLocal(list, m_root);
	}
	void TraversalPostOrder(LinkedList<T>& list)
	{
		if (m_root == nullptr)
			return;

		PostOrderLocal(list, m_root);
	}


private:
	//Traversal methods
	bool PostOrderLocal(LinkedList<T>& l, Node<T>* c)
	{
		if (c->leftNode)
		{
			if (PostOrderLocal(l, c->leftNode))
			{
				if (c->rightNode)
				{
					if (PostOrderLocal(l, c->rightNode))
					{
						l.PushBack(c->value);
						return true;
					}
				}
			}
		}
		else
		{
			if (c->rightNode)
			{
				if (PostOrderLocal(l, c->rightNode))
				{
					l.PushBack(c->value);
					return true;
				}
			}
			else
			{
				l.PushBack(c->value);
				return true;
			}
		}
	}
	bool InOrderLocal(LinkedList<T>& l, Node<T>* c)
	{
		if (c->leftNode)
		{
			if (InOrderLocal(l, c->leftNode))
			{
				l.PushBack(c->value);

				if (c->rightNode)
				{
					InOrderLocal(l, c->rightNode);
				}
			}
		}
		else
		{
			l.PushBack(c->value);

			if (c->rightNode)
			{
				InOrderLocal(l, c->rightNode);
			}
			return true;
		}
	}
	bool PreOrderLocal(LinkedList<T>& l, Node<T>* c)
	{
		if (c->leftNode)
		{
			l.PushBack(c->value);

			if (PreOrderLocal(l, c->leftNode))
			{
				if (c->rightNode)
				{
					PreOrderLocal(l, c->rightNode);
				}
			}
		}
		else
		{
			l.PushBack(c->value);

			if (c->rightNode)
			{
				PreOrderLocal(l, c->rightNode);
			}
			return true;
		}
	}

	//InsertLocal
	void InsertLocal(Node<T>* node, T value)
	{
		if (node->value == value)
		{
			return;
		}
		

		bool moveLeft = (value < node->value);

		if (moveLeft)
		{
			if (node->leftNode == nullptr)
			{
				node->leftNode = new Node<T>(value);
				return;
			}
			else
				InsertLocal(node->leftNode, value);
		}
		else
		{
			if (node->rightNode == nullptr)
			{
				node->rightNode = new Node<T>(value);
				return;
			}
			else
				InsertLocal(node->rightNode, value);
		}
	}

	//Remove
	void RemoveLocal(Node<T>* node)
	{
		if (node->leftNode != nullptr)
			RemoveLocal(node->leftNode);

		if (node->rightNode != nullptr)
			RemoveLocal(node->rightNode);


		delete node;
		node = nullptr;
	}

private:
	Node<T>* m_root;
	int m_size;
};