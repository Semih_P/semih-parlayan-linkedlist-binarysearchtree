#include "Node.h"

template <class T>
class LinkedList
{
//Methods that m ust be provided for linked list: push_front, push_back, pop_front,
//pop_back, clear, find and size

public:
	LinkedList()
	{
		m_root = nullptr;
		m_size = 0;
	}
	~LinkedList()
	{
		Clear();
	}

	void PushFront(T value)
	{
		//Add as m_root if there was no m_root
		if (m_root == nullptr)
		{
			m_root = new Node<T>(value);
			m_size++;
			return;
		}

		Node<T>* prevRoot = m_root;
		m_root = new Node<T>(value, m_root);
		prevRoot->leftNode = m_root;

		m_size++;
	}
	void PopFront()
	{
		//If there is no m_root, return
		if (m_root == nullptr)
			return;

		//If there is only one node, delete m_root
		if (m_root->rightNode == nullptr)
		{
			delete m_root;
			m_root = nullptr;
			m_size--;
			return;
		}

		//Delete m_root and set the rightNode to m_root
		Node<T>* nextRoot = m_root->rightNode;
		delete m_root;
		m_root = nextRoot;
		m_root->leftNode = nullptr;
		m_size--;
	}

	void PushBack(T value)
	{
		//Add as m_root if there was no m_root
		if (m_root == nullptr)
		{
			m_root = new Node<T>(value);
			m_size++;
			return;
		}

		//Aquire last active node in list and add a new node to the end
		Node<T>* endNode = LastNode();
		endNode->rightNode = new Node<T>(value, nullptr, endNode->leftNode);
		endNode->rightNode->leftNode = endNode;

		m_size++;
	}
	void PopBack()
	{
		//If there is no m_root, return
		if (m_root == nullptr)
			return;

		//Check if the last node is the same node as m_root
		Node<T>* endNode = LastNode();
		if (endNode->leftNode == nullptr)
		{
			delete m_root;
			m_root = nullptr;
			m_size--;
			return;
		}

		//Delete last node
		endNode->leftNode->rightNode = nullptr;
		delete endNode;
		m_size--;
	}

	T At(int index)
	{
		//If there is no m_root, return
		if (m_root == nullptr)
			return -1;

		//Check if index is within list boundaries
		if (index < 0 || index > Size() - 1)
		{
			return -1;
		}

		int i = 0;
		return AtLocal(m_root, i, index)->value;
	}

	int Size()
	{
		return m_size;
	}
	void Clear()
	{
		int numberOfPops = m_size;
		for (int i = 0; i < numberOfPops; i++)
		{
			PopBack();
		}
	}

	T First()
	{
		if (m_root == nullptr)
			return -1;

		return m_root->value;
	}
	T Last()
	{
		if (m_root == nullptr)
			return -1;

		return LastLocal(m_root)->value;
	}

	void PrintList()
	{
		Node<T>* currentNode = m_root;

		cout << "LinkedList: Size[" << Size() << "], Elements: ";
		while (true)
		{
			if (currentNode == nullptr)
				break;

			cout << currentNode->value;

			if (currentNode->rightNode != nullptr)
			{
				currentNode = currentNode->rightNode;
				cout << ", ";
			}
			else
			{
				break;
			}
		}
	}

private:
	Node<T>* LastNode()
	{
		if (m_root == nullptr)
			return nullptr;

		return LastLocal(m_root);
	}
	Node<T>* LastLocal(Node<T>* currentNode)
	{
		if (currentNode->rightNode == nullptr)
		{
			return currentNode;
		}
		else
		{
			return LastLocal(currentNode->rightNode);
		}
	}
	Node<T>* AtLocal(Node<T>* currentNode, int& i, int index)
	{
		if (i != index)
		{
			i++;
			currentNode = currentNode->rightNode;
			return AtLocal(currentNode, i, index);
		}
		else
		{
			return currentNode;
		}
	}

private:
	Node<T>* m_root;
	int m_size;
};